//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ServisSaltesa.Models.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Core.Objects;
    using System.Data.Entity.Infrastructure;

    public partial class Tipo_Comprobantes_Trans
    {
        [Key]
        [Column(Order = 1)]
        public int Tipo_Comprobante_id { get; set; }
        public byte Comprobante_ID { get; set; }
        public string Tipo_Comprobante_Serie { get; set; }
        public string Tipo_Comprobante_Numero { get; set; }
        public string Tipo_Comprobante_Secuencia { get; set; }
        public int Empresa_ID { get; set; }
        public string Comprobantes_Secuencia { get; set; }
        public bool Registro_Estado { get; set; }
        public string Registro_Usuario { get; set; }
        public System.DateTime Registro_Fecha { get; set; }
    
        public virtual Comprobantes_Cata Comprobantes_Cata { get; set; }
        public virtual Empresas_Trans Empresas_Trans { get; set; }


       



    }
}
