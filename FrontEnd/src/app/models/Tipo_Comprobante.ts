export class Tipo_Comprobante {

  Tipo_comprobante_id?: number;
  Comprobante_ID?: number;
  Tipo_Comprobante_Serie?: string;
  Tipo_Comprobante_Numero?: string;
  Tipo_Comprobante_Secuencia?: string;
  Comprobante_Secuencia?: string;
  Empresa_ID?: number;
  Registro_Estado?: number;
  Regidtro_Usuario?: string;
  Registro_Fecha?: Date;

}
