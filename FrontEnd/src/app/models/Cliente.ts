export class Cliente {

  public Cliente_id?: number
  public Cliente_Nombre?: string;
  public Cliente_Apellido?: string;
  public Cliente_Direccion?: string;
  public Cliente_Email?: string;
  public Cliente_Telefono?: string;
  public Cliente_Celular?: string
  public Cliente_RNC?: string
  public  Linea?: number;
  public  PageIndex?: number;
  public  PageSize?: number;
  public Ultima_Linea?: number;
  public orderByDirection0?: number;
  public Cantidad_Registros?: number;
  public  Registro_Estado?: boolean;
  public  Registro_Usuario?: string;
  public Registro_Fecha?: Date;




}
